terraform {
  backend "s3" {
    bucket         = "yossi-terraform-state-backend"
    key            = "devops-infra/eks/terraform.tfstate"
    region         = "eu-west-1"
  }
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
    kubernetes = {
      source  = "registry.terraform.io/hashicorp/kubernetes"
      version = ">= 1.11.1"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = var.region
}
